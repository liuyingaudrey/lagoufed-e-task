# 一、简答题

#### 1、Webpack 的构建流程主要有哪些环节？如果可以请尽可能详尽的描述 Webpack 打包的整个过程。

第一步：初始化参数：获取配置文件和命令行中的参数进行融合。

第二步：编译阶段：

- 将初始化后的参数传入创建好的compiler对象，给compiler挂载配置文件中的plugin。
- 通过nodeEnvironmentPlugin使compiler具有文件读写能力。
- 给compiler挂载默认插件，执行compiler的run方法。并找到编译入口文件。
- 根据入口文件生成一个chunk。
- 根据main chunk构建所有的依赖模块。
- 生成chunk assets资源列表。
- 合并chunk assets

第三步：完成输出:根据配置的路径和文件名，将文件写入指定路径

#### 2、Loader 和 Plugin 有哪些不同？请描述一下开发 Loader 和 Plugin 的思路。

　loader:是文件加载器，可以对特定类型的文件进行编译、压缩、转换等处理。loader的执行顺序从后往前执行的。第一个执行的loader接收源文件内容作为参数，其他的loader接收前一个loader的返回值作为参数。

​	plugin：是webpack运行周期中在对应节点执行比如资源管理，目录清除等操作的。

​	区别：loader只是对特定的文件进行转换。而plugin是基于事件机制工作，监听webpack执行打包中的节点，执行任务。丰富了webpack本身。

Loader 开发思路：

- module.exports导出一个函数。
- 这个函数接收source作为参数。
- 该函数返回在函数体内处理后的资源。

Plugin开发思路：

- 定义一个类
- 这个类中有一个apply方法
- apply接收compiler作为参数
- 在compiler的钩子上注册监听，通过webpack提供的api获取资源做处理(compilation.assets)
- 　再处理完后的资源返回(处理完以后再重新赋值给compilation.assets)。

# 二、编程题

#### 1、使用 Webpack 实现 Vue 项目打包任务

`webpack.common.js`配置

入口和输出

```js
entry: "./src/main.js",
output: {
    filename: "bundle-[chunkhash:8].js",
    path: path.join(__dirname, "dist")
},
```

loader配置

```js
module: {
        rules: [
            //配置babel-loader 解析js
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            },
            //配置eslint-loader 检查js文件
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: "eslint-loader",
                enforce: "pre"
            },
            //配置laoder解析css文件
            {
                test: /\.css/,
                use: ["style-loader", "vue-style-loader", "css-loader"]
            },
            //配置less解析less文件
            {
                test: /\.less/i,
                use: ["style-loader", "css-loader", "less-loader"]
            },
            //解析图片文件 首先用url-loader转换 如果超过limit 则用file-loader
            {
                test: /\.(png|jpg|gif)$/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            fallback: "file-loader",
                            esModule: false
                        }
                    }
                ]
            },
            //解析.vue文件
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
```

plugin配置

```js
plugins: [
    //编译项目中的 html 类型的文件
    new HtmlWebpackPlugin({
        title: "my-vue-webpack",
        template: "./public/index.html",
        templateParameters: {
            BASE_URL: '/'
        }
    }),
    //编译.vue文件时不仅需要vue-loader 还必须配置plugins
    new VueLoaderPlugin()
]
```

`webpack.dev.js`配置

```js
const { merge } = require("webpack-merge") //合并webpack的配置项
const common = require("./webpack.common")
const webpack = require("webpack")
module.exports = merge(common, {
    mode: "development", 
    devtool: "eval-cheap-module-source-map",
    //webpack-dev-server配置
    devServer: {
        contentBase: "./dist",
        // open: "Chrome"
    },
    plugins: [
        //热模块更新
        new webpack.HotModuleReplacementPlugin()
    ]
})
```

`webpack.prod.js`配置

```js
const { merge } = require("webpack-merge")
const common = require("./webpack.common")
//build之前先clean清除dist目录
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
//拷贝资源
const CopyPlugin = require("copy-webpack-plugin")

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                { from: "public/favicon.ico", 
                 to: "favicon-[chunkhash:8].ico" 
                }
            ]
        })
    ],
    //优化
    optimization: {
        // 模块只导出被使用的成员
        usedExports: true,
        // 压缩输出结果
        minimize: true,
        // 尽可能合并每一个模块到一个函数中
        concatenateModules: true,
        // 自动提取所有公共模块到单独 bundle
        splitChunks: {
            chunks: 'all'
        }
    }
})
```

`package.json`

```json
{
  "name": "vue-app-base",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "serve": "webpack-dev-server --config webpack.dev.js --open",
    "build": "webpack --config webpack.prod.js",
    "lint": "eslint --ext .js,.vue src --fix && stylelint src/**/*.less"
  },
  "dependencies": {
    "core-js": "^3.6.5",
    "vue": "^2.6.11",
    "webpack-cli": "^3.3.12"
  },
  "devDependencies": {
    "@babel/core": "^7.13.10",
    "@babel/preset-env": "^7.13.10",
    "@vue/cli-plugin-babel": "^4.5.12",
    "babel-loader": "^8.2.2",
    "clean-webpack-plugin": "^3.0.0",
    "copy-webpack-plugin": "^8.0.0",
    "css-loader": "^5.1.3",
    "eslint": "^7.22.0",
    "eslint-config-standard": "^16.0.2",
    "eslint-loader": "^4.0.2",
    "eslint-plugin-import": "^2.22.1",
    "eslint-plugin-node": "^11.1.0",
    "eslint-plugin-promise": "^4.3.1",
    "eslint-plugin-vue": "^7.7.0",
    "file-loader": "^6.2.0",
    "html-webpack-plugin": "^5.3.1",
    "less": "^4.1.1",
    "less-loader": "^8.0.0",
    "style-loader": "^2.0.0",
    "stylelint": "^13.12.0",
    "stylelint-config-standard": "^21.0.0",
    "url-loader": "^4.1.1",
    "vue-loader": "^15.9.6",
    "vue-template-compiler": "^2.6.12",
    "webpack": "^5.26.2",
    "webpack-dev-server": "^3.11.2",
    "webpack-merge": "^5.7.3"
  },
  "eslintConfig": {
    "root": true,
    "env": {
      "node": true
    },
    "extends": [
      "plugin:vue/essential",
      "eslint:recommended"
    ],
    "parserOptions": {
      "parser": "babel-eslint"
    },
    "rules": {}
  },
  "browserslist": [
    "> 1%",
    "last 2 versions",
    "not dead"
  ]
}

```



