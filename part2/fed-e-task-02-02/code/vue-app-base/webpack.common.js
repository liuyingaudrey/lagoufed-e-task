const path = require("path")
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require("html-webpack-plugin")
module.exports = {
    entry: "./src/main.js",
    output: {
        filename: "bundle-[chunkhash:8].js",
        path: path.join(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: "eslint-loader",
                enforce: "pre"
            },
            {
                test: /\.css/,
                use: ["style-loader", "vue-style-loader", "css-loader"]
            },
            {
                test: /\.less/i,
                use: ["style-loader", "css-loader", "less-loader"]
            },
            {
                test: /\.(png|jpg|gif)$/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            fallback: "file-loader",
                            esModule: false
                        }
                    }
                ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "my-vue-webpack",
            template: "./public/index.html",
            templateParameters: {
                BASE_URL: '/'
            }
        }),
        new VueLoaderPlugin()
    ]
}