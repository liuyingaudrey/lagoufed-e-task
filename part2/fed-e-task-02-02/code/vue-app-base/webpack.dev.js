const { merge } = require("webpack-merge")
const common = require("./webpack.common")
const webpack = require("webpack")
module.exports = merge(common, {
    mode: "development",
    devtool: "eval-cheap-module-source-map",
    devServer: {
        contentBase: "./dist",
        // open: "Chrome"
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
})