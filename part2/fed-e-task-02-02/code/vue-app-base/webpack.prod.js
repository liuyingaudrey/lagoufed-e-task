const { merge } = require("webpack-merge")
const common = require("./webpack.common")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const CopyPlugin = require("copy-webpack-plugin")

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                { from: "public/favicon.ico", to: "favicon-[chunkhash:8].ico" }
            ]
        })
    ],
    optimization: {
        // 模块只导出被使用的成员
        usedExports: true,
        // 压缩输出结果
        minimize: true,
        // 尽可能合并每一个模块到一个函数中
        concatenateModules: true,
        // 自动提取所有公共模块到单独 bundle
        splitChunks: {
            chunks: 'all'
        }
    }

})