## 一、简答题

### 1、当我们点击按钮的时候动态给 data 增加的成员是否是响应式数据，如果不是的话，如何把新增成员设置成响应式数据，它的内部原理是什么。

```js
let vm = new Vue({
 el: '#el'
 data: {
  o: 'object',
  dog: {}
 },
 method: {
  clickHandler () {
   // 该 name 属性是否是响应式的
   this.dog.name = 'Trump'
  }
 }
})
```

答：不是响应式的。可调用`this.$set(this.dog,"name","Trump")` 来实现。

内部原理：调用`Object.defineProperty()`将其设置为响应式。

### 2、请简述 Diff 算法的执行过程

 答：Diff 算法的执行过程：在`init`方法返回的`patch`方法中先判断`oldVnode`和`vnode`是否相同；如果不相同，通过`createElm`方法创建`vnode`的真实节点，挂载到`vnode`的 el 上，找到`oldVnode`的下一个兄弟节点，在此兄弟节点前插入`vnode.el`,删除`oldVnode`。

如果`oldVnode`和`vnode`相同。调用`patchVnode`方法对比新旧节点。

```js
return function patch (oldVnode: VNode | Element, vnode: VNode): VNode {
    let i: number, elm: Node, parent: Node
    const insertedVnodeQueue: VNodeQueue = []
    for (i = 0; i < cbs.pre.length; ++i) cbs.pre[i]()

    if (!isVnode(oldVnode)) {
        oldVnode = emptyNodeAt(oldVnode)
    }
    if (sameVnode(oldVnode, vnode)) { //新旧节点相同
        patchVnode(oldVnode, vnode, insertedVnodeQueue)
    } else { //新旧节点不同
        elm = oldVnode.elm!
            parent = api.parentNode(elm) as Node

        createElm(vnode, insertedVnodeQueue)

        if (parent !== null) {
            api.insertBefore(parent, vnode.elm!, api.nextSibling(elm))
            removeVnodes(parent, [oldVnode], 0, 0)
        }
    }

    for (i = 0; i < insertedVnodeQueue.length; ++i) {
        insertedVnodeQueue[i].data!.hook!.insert!(insertedVnodeQueue[i])
    }
    for (i = 0; i < cbs.post.length; ++i) cbs.post[i]()
    return vnode
}
```

`patchVnode`

先判断`vnode`是否定义 text;若定义，判断`oldVnode.text`和`vnode.text`是否相同，若不同，删除`oldVnode`的子节点，设置成`vnode.text`

若`vnode`没有定义 text,而新旧节点都有子节点，且子节点不相同，调用`updateChildren`；若`vnode`有子节点，且`oldVnode`是有`oldVnode.text`,则删除`oldVnode.text`,给`oldVnode`增加`vnode`的子节点；若`oldVnode`有子节点，删除`oldVnode`的子节点；若`oldVnode`有 text,删除。

```js
function patchVnode (oldVnode: VNode, vnode: VNode, insertedVnodeQueue: VNodeQueue) {
    const hook = vnode.data?.hook
    hook?.prepatch?.(oldVnode, vnode)
    const elm = vnode.elm = oldVnode.elm!
    const oldCh = oldVnode.children as VNode[]
    const ch = vnode.children as VNode[]
    if (oldVnode === vnode) return
    if (vnode.data !== undefined) {
      for (let i = 0; i < cbs.update.length; ++i) cbs.update[i](oldVnode, vnode)
      vnode.data.hook?.update?.(oldVnode, vnode)
    }
    //判断`vnode`是否定义text
    if (isUndef(vnode.text)) {
      if (isDef(oldCh) && isDef(ch)) {
        if (oldCh !== ch) updateChildren(elm, oldCh, ch, insertedVnodeQueue)
      } else if (isDef(ch)) {
        if (isDef(oldVnode.text)) api.setTextContent(elm, '')
        addVnodes(elm, null, ch, 0, ch.length - 1, insertedVnodeQueue)
      } else if (isDef(oldCh)) {
        removeVnodes(elm, oldCh, 0, oldCh.length - 1)
      } else if (isDef(oldVnode.text)) {
        api.setTextContent(elm, '')
      }
    } else if (oldVnode.text !== vnode.text) {
       //`oldVnode.text`和`vnode.text`不相同
      if (isDef(oldCh)) {
        removeVnodes(elm, oldCh, 0, oldCh.length - 1)
      }
      api.setTextContent(elm, vnode.text!)
    }
    hook?.postpatch?.(oldVnode, vnode)
  }
```

`updateChildren`

在进行同级别节点比较的时候，首先会对新旧节点数组的开始和结束节点设置标记索引，遍历过程中移动索引

在对开始和结束节点比较的时候，总共有四种情况：

```js
 oldStartVnode / newStartVnode(旧开始节点 / 新开始节点)
 oldEndVnode / newEndVnode(旧结束节点 / 新结束节点)
 oldStartVnode / newEndVnode(旧开始节点 / 新结束节点)
 oldEndVnode / newStartVnode(旧结束节点 / 新开始节点) 循环遍历新旧节点
```

首先判断 oldStartVnode 和 newStartVnode 是否是 sameVnode(el 和 key 是否相同)

```js
patchVnode 比较 oldStartVnode 和 newStartVnode，更新节点
标记索引往后移(++oldStartIdx、++newStartIdx)
```

否则判断 oldEndVnode 和 newEndVnode 是否是 sameVnode(el 和 key 是否相同)

```js
 patchVnode 比较 oldEndVnode 和 newEndVnode，更新节点
 标记索引往前移(--oldEndIdx、--newEndIdx)
```

否则判断 oldStartVnode 和 newEndVnode 是否是 sameVnode(el 和 key 是否相同)

```js
 patchVnode 比较 oldStartVnode 和 newEndVnode，更新节点
 把 oldStartVnode 移动到右边
 标记索引移动(++oldStartIdx、--newEndIdx)
```

否则判断 oldEndVnode 和 newStartVnode 是否是 sameVnode(el 和 key 是否相同)

```
patchVnode 比较 oldEndVnode 和 newStartVnode，更新节点
把 oldEndVnode 移动到左边
标记索引移动(--oldEndIdx、++newStartIdx)
```

如果不是以上四种情况

```js
 遍历新节点，使用 newStartVnode 的 key 在老节点数组中找相同节点
 如果没有找到，说明 newStartVnode 是新节点
 创建新节点对应的 DOM 元素，插入到 DOM 树中
 如果找到了
 判断新节点和找到的老节点的 sel 选择器是否相同
 如果不相同，说明节点被修改了
 重新创建对应的 DOM 元素，插入到 DOM 树中
 如果相同，把 elmToMove 对应的 DOM 元素，移动到左边

 循环结束
 当老节点的所有子节点先遍历完(oldStartIdx > oldEndIdx)，循环结束
 新节点的所有子节点先遍历完(newStartIdx > newEndIdx)，循环结束
 如果老节点的数组先遍历完(oldStartIdx > oldEndIdx)，说明新节点有剩余，把剩余节点批量插入到右边
 如果新节点的数组先遍历完(newStartIdx > newEndIdx)，说明老节点有剩余，把剩余节点批量删除

```

## 二、编程题

### 1、模拟 VueRouter 的 hash 模式的实现，实现思路和 History 模式类似，把 URL 中的 # 后面的内容作为路由的地址，可以通过 hashchange 事件监听路由地址的变化。

答：history 模式主要是使用 pushState 来记录历史，用 popState 来更新页面内容；hash 模式主要使用 hashChange 监听 hash 变化，更改当前路由。
详见代码vuerouter-handwriting。

### 2、在模拟 Vue.js 响应式源码的基础上实现 v-html 指令，以及 v-on 指令。

答：详见代码miniVue。

### 3、参考 Snabbdom 提供的电影列表的示例，利用 Snabbdom 实现类似的效果。

答：详见代码snabbdom-movie-demo。


