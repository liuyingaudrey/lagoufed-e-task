class Observer {
    constructor(data) {
        this.walk(data)
    }
    walk(data) {
        // 1.判断data是否为对象 如果是空或者不是对象 不做处理
        if (!data || typeof data !== "object") {
            return
        }
        // 2.遍历data对象的属性 给data属性中的每一项都添加getter和setter
        Object.keys(data).forEach(key => {
            this.defineReactive(data, key, data[key])
        })
    }
    defineReactive(data, key, val) {
        let that = this
        //负责收集依赖 并发送通知
        let dep = new Dep()
        //如果data中的某个属性的值也是对象做递归处理
        this.walk(val)
        Object.defineProperty(data, key, {
            enumerable: true,
            configurable: true,
            get() {
                //收集依赖
                Dep.target && dep.addSub(Dep.target)
                return val
            },
            set(newVal) {
                if (newVal === val) {
                    return
                }
                val = newVal
                //如果设置的值是对象 也要做劫持
                that.walk(newVal)
                //发送通知
                dep.notify()
            }
        })
    }
}