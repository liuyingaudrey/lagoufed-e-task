class Vue {
    constructor(options) {
        //通过属性保存选项的数据
        this.$options = options || {}
        this.$data = options.data || {}
        this.$el = typeof options.el === "string" ? document.querySelector(options.el) : options.el
        //将data转换成getter setter注入到vue实例上
        this._proxyData(this.$data)
        //调用Observer对象 监听数据变化
        new Observer(this.$data)
        //调用Compiler对象 传入vm实例 解析指令和插值表达式
        new Compiler(this)
    }
    _proxyData(data) {
        //遍历data中的每一项 且将灭一项都转成getter和setter注入到vue实例
        Object.keys(data).forEach(key => {
            Object.defineProperty(this, key, {
                enumerable: true,
                configurable: true,
                get() {
                    return data[key]
                },
                set(newVal) {
                    if (newVal === data[key]) {
                        return
                    }
                    data[key] = newVal
                }
            })
        })
    }
}