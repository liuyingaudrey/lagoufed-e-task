class Watcher {
    constructor(vm, key, cb) {
        this.vm = vm
        this.key = key
        //数据变化时 调用cb更新视图
        this.cb = cb
        //在Dep的静态属性上记录当前watcher对象
        //当访问数据的时候把watcher添加到dep的subs中
        Dep.target = this
        //每触发一次getter 让dep为当前key记录watcher
        this.oldValue = vm[key]
        //清空target
        Dep.target = null
    }
    update() {
        const newValue = this.vm[this.key]
        if (this.oldValue === newValue) {
            return
        }
        this.cb(newValue)
    }
}