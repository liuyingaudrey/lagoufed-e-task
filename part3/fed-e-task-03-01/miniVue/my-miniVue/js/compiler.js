//负责解析指令/插值表达式
class Compiler {
    constructor(vm) {
        this.el = vm.$el
        this.vm = vm
        //调用compile编译模板
        this.compile(this.el)
    }
    compile(el) {
        const nodes = el.childNodes
        //转成数组遍历
        Array.from(nodes).forEach(node => {
            //判断node的类型 是元素节点还是文本节点
            if (this.isTextNode(node)) {
                //插值表达式
                this.compileText(node)
            } else if (this.isElementNode(node)) {
                //指令
                this.compileElement(node)
            }
            //如果node还有子节点 递归编译
            if (node.childNodes && node.childNodes.length) {
                this.compile(node)
            }
        })
    }
    //处理插值表达式
    compileText(node) {
        let reg = /\{\{(.+?)\}\}/
        let value = node.textContent
        if (reg.test(value)) {
            let key = RegExp.$1.trim()
            node.textContent = value.replace(reg, this.vm[key])
            //创建watcher对象 当数据改变时更新视图
            new Watcher(this.vm, key, newVal => {
                node.textContent = newVal
            })
        }
    }

    compileElement(node) {
        Array.from(node.attributes).forEach(attr => {
            let attrName = attr.name
            if (this.isDerective(attrName)) {
                attrName = attrName.substr(2) //去掉v-
                console.log(attrName)
                let key = attr.value
                if (attrName.includes(":")) {
                    let eventType = attrName.substr(3)
                    this.eventUpdate(node, key, eventType)
                } else {
                    this.update(node, key, attrName)
                }
            }
        })
    }
    //处理v-on
    eventUpdate(node, key, eventType) {
        node.addEventListener(eventType, this.vm.$options.methods[key], false)
    }
    //负责更新dom 创建watch
    update(node, key, attrName) {
        let updateFn = this[attrName + "Updater"]
        //因为在textUpdater等中要使用this 所以改为call调用
        updateFn && updateFn.call(this, node, this.vm[key], key, attrName)
    }
    //处理v-text
    textUpdater(node, value, key) {
        node.textContent = value
        //每一个指令中创建一个watcher,观察数据的变化
        new Watcher(this.vm, key, newVal => {
            node.textContent = newVal
        })
    }
    //处理v-model
    modelUpdater(node, value, key) {
        node.value = value
        new Watcher(this.vm, key, newVal => {
            node.value = newVal
        })
        //双向绑定
        node.addEventListener("input", () => {
            this.vm[key] = node.value
        })
    }
    //处理v-html
    htmlUpdater(node, value, key) {
        node.innerHTML = value
        new Watcher(this.vm, key, newVal => {
            node.innerHTML = newVal
        })
    }
    //判断元素属性是否为指令
    isDerective(attrName) {
        return attrName.startsWith("v-")
    }
    //是否为文本节点
    isTextNode(node) {
        return node.nodeType === 3
    }
    //是否为元素节点
    isElementNode(node) {
        return node.nodeType === 1
    }
}