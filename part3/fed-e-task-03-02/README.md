## Vue.js 源码剖析-响应式原理、虚拟 DOM、模板编译和组件化

### 简答题

#### 1、请简述 Vue 首次渲染的过程。

　答：首先进行`new Vue()`操作时，会调用Vue构造函数中的`_init()`方法对vue实例进行初始化操作: 

- 合并options(`vm.$options = mergeOptions()`)
- vm生命周期相关变量初始化（`initLifecycle(vm)`）
- vm事件监听初始化（`initEvents(vm)`）
- vm的编译render初始化(`initRender(vm)`)
- 触发`beforeCreated()`(`callHook(vm,'beforeCreate')`)
- 把inject的成员注入到vm上（`initInjections(vm)`）
- 初始化vm的`_props/methods/_data/computed/watch`(`initState(vm)`)
- 初始化provide(`initProvide(vm)`)
- 触发created(`callHook(vm,'created')`)
- ***调用$mount()挂载（`vm.$mount(vm.$options.el)`）***

***`Vue.prototype.$mount`方法:***

- 判断$options中是否传入了render函数;如果没有，判断是否传入了template；如果没有template,获取el的outerHTML作为template。然后将template通过`compileToFunction()`转换成render函数，存入vm.options.render中;
- **return mount.call(this, el, hydrating)**

**`Vue.prototype.$mount`方法:**

- 传入el,处理了el。
- 返回`mountComponent(this,el,hydrating)`　。

`mountComponent(this,el,hydrating)`方法：

- 触发`callHook(vm, 'beforeMount')`。

- 定义`updateComponent`

- 根据条件生成`updateComponent`: `updateComponnet = function(){vm._update(vm._render(),hydrating)}`;`vm._render()`生成虚拟dom 返回vnode;

  vm._update方法将vnode渲染成真实dom （vm.__patch__()）。首次渲染会调用  数据更新也会调用。

  _update 方法是在lifecycleMixin中定义。

- `new Watcher(vm,updateComponent,noop,{})`  创建watcher的时候会调一次watcher的get方法。

- 触发`callHook(vm, 'mounted')`

- 返回vm实例

![vue](D:\LearningFiles\P7\Homework\lagoufed-e-task\part3\fed-e-task-03-02\vue.png)

#### 2、请简述 Vue 响应式原理。

- new Vue()进入vue构造函数； 调用`this._init(options)`方法，进入_init()方法

- `_init()`方法中，调用initState(vm)初始化`props、methods、data、computed、watch`,初始化data调用initData(),进入initData()

- initData()中的操作：

  - 拿到data后获取data中的所有属性 keys = Object.keys(data)
  - 判断data上的成员是否和props/methods重名
  - 调用`observe(data,true/*rootData*/)`进行响应式处理

- observe(value,boolean)中的操作：

  - 先判断传入的data值是不是对象，如果不是/是vnode的实例，直接返回。
  - 返回一个ob = new Observer(value)。

- Observer类的constructor过程：

  - 存入value `this.value = value`
  - 实例化Dep `this.dep = new Dep()`
  - 记录初始化实例的个数： `this.vmCount = 0`
  - 如果data是数组，传入data,为数组中的每一个对象创建一个observer实例 `this.observerArray(value)`
  - 如果data是对象，传入data,遍历对象中的每一个属性，转成setter/getter `this.walk(value)`

- Observer类中的`walk(obj)`方法

  - 获取对象的每一个属性`keys=Object.key(obj)`
  - 遍历每一个属性 设置为响应式数据 `defineReactive(obj,keys[i])`

- `defineReactive(obj,key,val,customSetter,shallow)`方法（与`Observer`类同一个页面定义的方法）：用`Object.defineProperty`将data中的每一项设置都设置`get`和`set`成为响应式

  - ```js
    // 创建依赖对象实例
     const dep = new Dep()
    // 提供预定义的存取器函数
      const getter = property && property.get
      const setter = property && property.set
      if ((!getter || setter) && arguments.length === 2) {
        val = obj[key]
      }
    //如果data中的某一项是对象 递归调用observe(val)添加getter/setter.返回子观察对象。
    let childOb = !shallow && observe(val)
    Object.defineProperty(obj,key,{
        enumerable:true,
        configurable:true,
        get:function reactiveGetter(){
            //如果预定义的getter存在则value等于getter调用的返回值
            //否则直接赋予属性值
            const value = getter ? getter.call(obj) : val
            //如果存在当前依赖目标 则建立依赖
            if(Dep.target){
                dep.depend()
                //如果子观察对象存在 建立子对象的依赖关系
                if(childOb){
                    childOb.dep.depend()
                    //如果属性是数组，则特殊处理收集数组对象依赖
                    if(Array.isArray(value)){
                        dependArray(value)
                    }
                }
            }
            //返回属性
            return value
        },
        set:function reactiveSetter(newVal){
            const value = gettter ? getter.call(obj) :val
            //如果新值等于旧值 或者 新值旧值为NaN则不执行
            if(newVal === val || (newVal !== newVal && value !== value)){
                return
            }
            
            if (process.env.NODE_ENV !== 'production' && customSetter) {
                customSetter()
            }
            //如果没有setter直接返回
            if(g		etter && !setter) return
            //如果预定义setter存在则调用 否则直接更新心智
            if(setter){
                setter.call(obj,newVal)
            }else{
                val = newVal
            }
            //如果新值时对象 观察子对象并返回子对象的observer对象
            childOb = !shallow && observe(newVal)
            //派发通知
            dep.notify()
        },
    })
    ```

- `observerArray(Array) `:为数组中的每一项创建一个observer实例
  
  - 循环遍历每一项，传入oberve(items[i])

响应式原理：
![reactive](D:\LearningFiles\P7\Homework\lagoufed-e-task\part3\fed-e-task-03-02\reactive.png)



#### 3、请简述虚拟 DOM 中 Key 的作用和好处。

　答：Key是用来比较Diff算法中vnode是否是相同节点，如果不设置key，会最大程度地重用dom元素，但是有时候重用dom元素会出现渲染错误的问题。给具有相同父元素的子元素设置唯一值key后，就会避免出现渲染错误的情况。

Diff算法的核心在于同级别节点比较，key就是用来判断同级别新旧节点是否相同。

　当修改这些同级别节点的某个内容、变更位置、删除、添加等时，需要更新视图，Vue调用patch方法对比新旧节点。如果新旧节点相同，则调用patchVnode。

​	patchVode：若新节点无文本节点，会删除旧节点的子节点或文本，将新节点的子节点渲染到旧节点的子节点位置。若新旧节点都有子节点，则对二者的子节点进行Diff算法，调用updateChildren,此时Key就起了作用：

- 若子节点未设置Key,新旧节点在执行sameVnode时就认为二者相同，再次执行patchVnode对比他们的子节点。
- 若设置了Key，当执行sameVnode时：
  - 若Key不同，sameVnode返回false，继续后面的操作。
  - 若Key相同，再次执行patchVnode对比他们的子节点。
- 设置key和不设置key相比，会减少调用patchVnode的次数。

#### 4、请简述 Vue 中模板编译的过程。

　在`Vue.prototype.$mount`方法中，`const {render, staticRenderFns} = compileToFunction(template,{},this)`

`compileToFunction(template,options,vm)`

- 读取缓存中的模板编译对象，如果有，直接读取；
- 如果没有，调用`comile(template,options)`将模板编译成字符串形式的js代码；
- 调用`createFunction(compiled.render,fnGenErrors)`将字符串的js转换成js方法；
- 缓存并返回模板编译对象。`res.render `和`res.staticRenderFns`
- `render`和`staticRenderFns`初始化完毕后，挂载到vm的options对应的属性中。
- 最终在`vue.prototype._render()`方法中调用`render()`生成vnode.

`compile(template,options)`:

- `const compiled = baseCompile(tempalte.trim(),finalOptions)`;
- `return  compiled`

`baseCompile(template,options)`

- 把模板转换成ast抽象语法树；`const ast = parse(template.trim(),options)`;
- 优化语法抽象语法树； `optimize(ast,options)`
- 把抽象语法树生成的字符串形式的js代码； `const code = generate(ast,options)`
- 返回`{ast,render:code.render,staticRenderFns:code.staticRenderFns}`

![compile](D:\LearningFiles\P7\Homework\lagoufed-e-task\part3\fed-e-task-03-02\compile.png)