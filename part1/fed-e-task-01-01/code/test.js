const MyPromise = require("./MyPromise")

function p1(){
    return new MyPromise((res,rej)=>{
        // res("hello")
        rej("failed")
    })
}
/* p1().then(val=>{
    console.log(val)
},error=>console.log(error)) */
p1().catch(val=>console.log(val))