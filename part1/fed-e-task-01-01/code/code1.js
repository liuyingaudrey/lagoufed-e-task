/*
  将下面异步代码使用 Promise 的方法改进
  尽量用看上去像同步代码的方式
  setTimeout(function () {
    var a = 'hello'
    setTimeout(function () {
      var b = 'lagou'
      setTimeout(function () {
        var c = 'I ♥ U'
        console.log(a + b +c)
      }, 10)
    }, 10)
  }, 10)
*/

function p1(){
  return new Promise(function(resolve,reject){
    var a = "hello",b="lagou",c="I ♥ U"
    resolve(a+b+c)
  })
}
p1().then(val=>console.log(val))
