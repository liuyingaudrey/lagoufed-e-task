/*
尽可能还原 Promise 中的每一个 API, 并通过注释的方式描述思路和原理.
*/
//1.Promise是一个类 执行类的时候传递一个执行器 立即执行
//2.有三个状态：

const { set } = require("lodash")

//3.resolve和reject改变状态 resolve->fulfilled reject->rejected
const PENDING = "pengding"
const FULFILLED = "fulfilled"
const REJECTED = "rejected"
class MyPromise{
    constructor(executor){
        try{
            executor(this.resolve,this.reject)
        }catch(e){
            this.reject(e)
        }
    }
    //初始状态
    status = PENDING
    //成功之后的值
    value = undefined
    //失败原因
    reason = undefined
    //成功回调
    successCallback = [];
    //失败回调
    failCallback = [];
    resolve = value=>{
        //一旦状态改变 不可改变
        if(this.status !== PENDING) return;
        //状态改为成功
        this.status = FULFILLED
        //保存成功之后的值
        this.value = value
        //成功回调
        while(this.successCallback.length) this.successCallback.shift()()
    }
    reject = reason=>{
        //一旦状态改变 不可改变
        if(this.status !== PENDING) return;
        //状态改为失败
        this.status = REJECTED
        //保存失败的原因
        this.reason = reason
        //失败回调
        while(this.failCallback.length) this.failCallback.shift()()
    }
    then(successCallback,failCallback){
        successCallback = successCallback?successCallback:value=>value;
        failCallback = failCallback?failCallback:reason=>{throw reason}
        let promise2 = new MyPromise((resolve,reject)=>{
            //判断状态
            if(this.status === FULFILLED){
                setTimeout(()=>{
                    try {
                        let x = successCallback(this.value);
                        // 判断 x 的值是普通值还是promise对象
                        // 如果是普通值 直接调用resolve 
                        // 如果是promise对象 查看promsie对象返回的结果 
                        // 再根据promise对象返回的结果 决定调用resolve 还是调用reject
                        resolvePromise(promise2,x,resolve,reject)
                    } catch (error) {
                        reject(error)
                    }
                },0)
            }else if(this.status === REJECTED){
                setTimeout(()=>{
                    try {
                        let x = failCallback(this.reason);
                        resolvePromise(promise2,x,resolve,reject)
                    } catch (error) {
                        reject(error)
                    }
                },0)
            }else{ //pending 将回调函数存起来 状态改变后再调用
                this.successCallback.push(()=>{
                    setTimeout(()=>{
                        try {
                            let x = successCallback(this.value);
                            resolvePromise(promise2,x,resolve,reject)
                        } catch (error) {
                            reject(error)
                        }
                    },0)
                })
                this.failCallback.push(()=>{
                    setTimeout(()=>{
                        try {
                           let x = failCallback(this.reason);
                           resolvePromise(promise2,x,resolve,reject)
                        } catch (error) {
                            reject(error)
                        }
                    },0)
                })
            }
        })
        return promise2
    }
    catch(failCallback){
        return this.then(undefined,failCallback)
    }
    finally(callback){
        return this.then(value=>{
            return MyPromise.resolve(callback()).then(()=>value)
        },reason=>{
            return MyPromise.reject(callback()).then(()=>{throw reason})
        })
    }
    static resolve(value){
        //如果是promise对象 直接返回
        if(value instanceof MyPromise) return value;
        //普通值
        return new Promise(resolve=>resolve(value))
    }
    static all(array){
        let result = [];
        let index = 0;
        function addData(key,value){
            result[key] = value;
            index++;
            if(index === array.length){
                resolve(result)
            }
        }
        return new MyPromise((resolve,reject)=>{
            for(let i=0;i<array.length;i++){
                let current = array[i];
                //promise对象
                if(current instanceof MyPromise){
                    current.then(value=>addData(i,value),reason=>reject(reason))
                }else{
                    addData(i,current)
                }
            }
        })
    }
}
function resolvePromise(promise2,x,resolve,reject){
    //如果返回promise2本身 报错
    if(promise2 === x){
        return reject(new TypeError("Chaining cycle detected for promise #<Promise>"))
    }
    //如果回调函数是一个promise对象
    if(x instanceof MyPromise){
        x.then(resolve,reject)
    }else{
        //普通值
        resolve(x)
    }
}
module.exports = MyPromise